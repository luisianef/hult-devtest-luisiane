# Hult Developer Test

App.js is the main js file where I fetch the data from the json file and call the components to display the data of the report.

## Components of Test
In src/components folder we can find the different classes that will be used for the repor:
1. Profile Class:  
Will display all the current information regarding the user.
2. ShortTermGoal Class:  
will display the short term goal information for the user report.
3. LongTermGoal class: 
will display the long term goal information for the user report.
4. Goals class: 
Used to create the information for the short and long term goals
5. Skills Class: 
Used to generate the desired skills for long and short term goals according to the already covered skills of the user
6. Skill class: 
display of skill information in the page.
7. Program class: 
display of the programs needed for long term goals in the page.

 