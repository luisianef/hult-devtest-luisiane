import React, { Component } from 'react';
import './App.css';
import Profile from "./components/Profile";
import ShortTermGoal from "./components/ShortTermGoal";
import LongTermGoal from "./components/LongTermGoal";


class App extends Component {
    constructor() {
        super();
        this.state = { 'user': [],
            'careerPath':[],
            showTextShort: false,  // Should the text be displayed?
            showTextLong: false  // Should the text be displayed?

        };
    }
    componentDidMount() {
        this.getItems()
    }
    getItems() {
        fetch("../data/report.json")
            .then(results => results.json())
            .then(data => this.setState({'user': data, 'careerPath':data.careerPath}))
            .catch(error => console.log(error));
    }

    handleClickShort = () => {
        console.log('Click happened');
        this.setState((state) => ({
            showTextShort: !state.showTextShort  //  Toggle showText
        }))
    }
    handleClickLong = () => {
        console.log('Click happened');
        this.setState((state) => ({
            showTextLong: !state.showTextLong  //  Toggle showText
        }))
    }
    render() {
        return (
            <div className="jumbotron text-center">
                <Profile user={this.state.user} />
                <div className="container--goals">
                    <button className="container--title" onClick={this.handleClickShort}>Your short-term goal</button>
                    {this.state.showTextShort && (<ShortTermGoal user={this.state.user} careerPath={this.state.careerPath}/>)}
                </div>
                <div className="container--goals">
                    <button className="container--title" onClick={this.handleClickLong}>Your Long-term goal</button>
                    {this.state.showTextLong && (<LongTermGoal user={this.state.user} careerPath={this.state.careerPath}/>)}
                </div>
            </div>
        );
    }
}

export default App;
