import React from 'react';
import './Profile.css';

class Profile extends React.Component {

    showUserHeader(firstname) {
        return firstname + "'s career path";
    }

   showUserInfo(firstname, lastname) {
        return (<p className="user">
            <i className="far fa-user circle-icon"/>
            <span className="circle-icon-card">{firstname} {lastname}</span>
        </p>);
   }
    showUserDetails(role, education, experience){
       return (<div className="container">
                   <div><p className="user">
                       <i className="fas fa-briefcase circle-icon"/>
                       <span className="circle-icon-card">{role}</span>
                    </p>
                    </div>
                   <div><p className="user">
                        <i className="fas fa-graduation-cap circle-icon"/>
                        <span className="circle-icon-card">{education}</span>
                    </p></div>
                   <div><p className="user">
                       <i  className="fas fa-clock circle-icon"/>
                       <span className="circle-icon-card">{experience} years experience</span></p>
                   </div>
           </div>
       );
   }


     render() {

        return (
            <div className="profile">
                <div className="container-fluid">
                     <h1>{this.showUserHeader(this.props.user.firstName)}</h1>
                </div>
                <div className="container">{this.showUserInfo(this.props.user.firstName, this.props.user.lastName)}</div>
                {this.showUserDetails(this.props.user.currentRole, this.props.user.education, this.props.user.experience)}
            </div>
         );
     }
}
export default Profile

