import React from 'react';
import './Goals.css';
import Goals from "./Goals";
import Programs from "./Programs"

class LongTermGoal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            showText: false  // Should the text be displayed?
        };
    }

    handleClick = () => {
        console.log('Click happened');
        this.setState((state) => ({
            showText: !state.showText  //  Toggle showText
        }))
    }

    render() {
        let size= this.props.careerPath.length;
        return (
            <div className="ontainer-fluid">
                {this.props.careerPath.slice(1,size).map((pathDetail, item)=> {
                    return (
                        <div>
                            <Goals pathDetail={pathDetail} currentRole={this.props.user.currentRoleDetails} goal={"Long"}/>

                            <button className="container--title" onClick={this.handleClick}>
                                    Programs that will help you to become a {pathDetail.name}</button>
                                {this.state.showText && (<Programs programs={this.props.user.programs}/>)}
                        </div>)
                })}

            </div>

        );
    }
}
export default LongTermGoal
