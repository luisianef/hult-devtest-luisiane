import React, { Component } from 'react';

class Programs extends Component {


    render() {
        let size= this.props.programs.length;

        return (
            <div className="row">
                {this.props.programs.slice(0,size).map((program, item)=> {
                    return (
                        <div className="col-sm-4">
                            <h3>Option {item+1} of {size}</h3>
                            <p className="role-user">{program.MasterTitle}</p>
                            <p className="role-user">{program.Description}</p>
                            <div className="well role-user"><a href={program.Link}>Go to Program</a></div>
                    </div>
                    )})}
            </div>
        );
    }
}

export default Programs;
