
import React, { Component } from "react";

class Skill extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showText: false  // Should the text be displayed?
        };
    }

    handleClick = () => {
        console.log('Click happened');
        this.setState((state) => ({
            showText: !state.showText  //  Toggle showText
        }))
    }
    render() {
        return (
            <div className="col-sm-4">
                <button className="skills-box" onClick={this.handleClick}>{this.props.skill.name}</button>
                {this.state.showText && <div>{this.props.skill.description}</div>}
            </div>
        )
    }
}

export default Skill;