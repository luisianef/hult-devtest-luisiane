import React, { Component } from 'react';
import Skill from "./Skill";



class Skills extends Component {

    getskills(skills, current, desiredskills){
        let desired = [];
        for (let skill of desiredskills){
            if(!current.includes(skill.name)){
                if(!skills.find(i => i.name == skill.name)) {
                    desired = desired.concat(skill)
                }
            }
        }
        return desired
    }

    showDesiredSkils(current, desiredskills){
        let desired= [];

        desired = (this.getskills(desired, current, desiredskills.baselineSkills))
        desired = desired.concat(this.getskills(desired,current, desiredskills.specializedSkills))
        desired = desired.concat(this.getskills(desired,current, desiredskills.distinguishingSkills))
        desired = desired.concat(this.getskills(desired,current, desiredskills.definingSkills))
        desired = desired.concat(this.getskills(desired, current, desiredskills.necessarySkills))

        return desired;
    }

    sortSkills(skills){
        skills.sort((a, b) => parseFloat(b.count) - parseFloat(a.count));
        return skills
    }
    topSkills(skills){
        return (<div className="row">{skills.slice(0,3)}</div>)


    }
    otherSkills(skills){
        return(<div className="row">{skills.slice(3,skills.length)}</div>);
    }

    longTermskillsforRole(skills){
        return(<div className="row">{skills}</div>);

    }
    render() {
        let skills = [];

        for (let skill of this.sortSkills(this.showDesiredSkils(this.props.selected, this.props.details.allSkills))) {
            skills.push(<Skill skill={skill}/>)
        }

        switch (this.props.goal)
        {
            case "Short":
                return (<div className="container-fluid">
                    <div className="row">
                        <h3>Top 3 most in-demand skills for this role</h3>
                        {this.topSkills(skills)}</div>
                        <div className="row">
                        <h3>Other skills typically needed for this role</h3>
                            {this.otherSkills(skills)}</div></div>);
            case "Long":
                return (<div className="row">
                    <h3>Other skills typically needed for this role</h3>
                    {this.longTermskillsforRole(skills)}</div>);

        }


    }

}

export default Skills;
