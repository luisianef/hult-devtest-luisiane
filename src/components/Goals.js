import React, { Component } from 'react';
import Skills from "./Skills";


class Goals extends Component {
    showRole(name) {
        return name;
    }
    showSalaryPercentage(A, B){
        return Math.round(100 * Math.abs( (A - B) / ( (A+B)/2 )));
    }
    showRoleDemand(demand){

        return (<div className={"role--" +(demand)}><i className="fas fa-chart-line"/><span className="role-user">{demand} market demand</span></div>)
    }

    render() {
        return (
            <div className="container">
                    <p className="role"><i className="fas fa-flag"></i><span className="role-user">
                        {this.props.pathDetail.name}</span></p>
                    <p className="user"><i className="fas fa-dollar-sign"></i><span className="role-user">
                        {this.showSalaryPercentage(this.props.currentRole.salaryMean,
                            this.props.pathDetail.details.meanSalary)} %</span>
                    </p>
                    <p className="user">
                        {this.showRoleDemand(this.props.pathDetail.details.demandLevel)}</p>
                    <p className="user"><i className="fas fa-clock"></i>
                        <span className="role-user">{this.props.pathDetail.details.experienceYears} year's experience</span></p>
                    <div className="user">
                        <Skills details={this.props.pathDetail.details} selected={this.props.pathDetail.selectedSkills} goal={this.props.goal}/>
                    </div>
            </div>
        );
    }
}

export default Goals;
