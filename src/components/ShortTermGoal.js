import React from 'react';
import './Goals.css';
import Goals from "./Goals";

class ShortTermGoal extends React.Component {

    render() {
        return (
            <div className="ontainer-fluid">
                {this.props.careerPath.slice(0,1).map((pathDetail, item)=> {
                    return (<Goals pathDetail={pathDetail}
                                   currentRole={this.props.user.currentRoleDetails} goal={"Short"}/>);
                })}
            </div>

        );
    }
}
export default ShortTermGoal
